INTRODUCTION
------------
This is an easy way to split a content for a specific group of site visitors. This module will split your site by a specific url prefix after installation and configuration. You can create multiple targets e.g. _Private_ accessible on example.com/en/private and _Professional_ accessible on example.com/en/professional.

Here is a couple of simple examples:

* Contact us page can contain different content per a target
* About page can be available only for one of the target e.g. Professional target example.com/en/professional/about
* Home page can have different layout and content per a target
* Site navigation can contain different links, some of them can be available for all targets

CONFIGURATION
-------------
* Enable the module.
* Create and configure your targets at /admin/structure/targets
* Provide additional settings at /admin/structure/targets/settings

MIGRATION PATH
--------------
Check out the detail migration path from the custom module dropsolid_targets to this one in the documentation listed on https://www.drupal.org/project/targets.

DOCUMENTATION
--------------
You can find a guide how to use the module for editors in the documentation listed on https://www.drupal.org/project/targets.

MAINTAINERS
-----------
Current maintainers:
* Alex Kuzava (nginex) - https://www.drupal.org/u/nginex
