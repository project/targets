<?php

/**
 * @file
 * Install/update Targets module.
 */

use Drupal\Core\Link;
use Drupal\targets\TargetInterface;

/**
 * Implements hook_requirements().
 */
function targets_requirements($phase) {
  $requirements = [];

  if ($phase === 'runtime') {
    $storage = \Drupal::entityTypeManager()->getStorage('target');
    $count = $storage->getQuery()->count()->execute();
    $requirements['targets']['title'] = t('Targets configuration');

    // Check number of available targets.
    if ($count > 0) {
      // Get labels of the targets.
      $targets = array_map(function (TargetInterface $target) {
        return $target->label();
      }, $storage->loadMultiple());

      // As requirement value use list of targets separated by comma.
      $requirements['targets']['value'] = implode(', ', array_values($targets));
    }
    else {
      $requirements['targets']['value'] = t('Not configured');
      $requirements['targets']['severity'] = REQUIREMENT_WARNING;
      $requirements['targets']['description'] = t('Targets are not configured. Follow this @link to configure targets.', [
        '@link' => Link::createFromRoute(t('link'), 'entity.target.collection')->toString(),
      ]);
    }
  }

  return $requirements;
}
