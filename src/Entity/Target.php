<?php

namespace Drupal\targets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\targets\TargetInterface;

/**
 * Defines the target entity.
 *
 * @ConfigEntityType(
 *   id = "target",
 *   label = @Translation("Target"),
 *   label_collection = @Translation("Targets"),
 *   label_singular = @Translation("target"),
 *   label_plural = @Translation("targets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count target",
 *     plural = "@count targets",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\targets\Form\TargetForm",
 *       "add" = "Drupal\targets\Form\TargetForm",
 *       "edit" = "Drupal\targets\Form\TargetForm",
 *       "delete" = "Drupal\targets\Form\TargetDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *     "list_builder" = "Drupal\targets\Controller\TargetListBuilder",
 *     "storage" = "Drupal\targets\TargetStorage",
 *   },
 *   admin_permission = "administer targets",
 *   config_prefix = "target",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/targets/add",
 *     "delete-form" = "/admin/structure/targets/manage/{target}/delete",
 *     "edit-form" = "/admin/structure/targets/manage/{target}",
 *     "collection" = "/admin/structure/targets",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "prefix",
 *     "default",
 *     "weight",
 *     "preferredLanguage",
 *   }
 * )
 */
class Target extends ConfigEntityBase implements TargetInterface {

  /**
   * The target ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The target label.
   *
   * @var string
   */
  protected $label;

  /**
   * The target prefix.
   *
   * @var string
   */
  protected $prefix;

  /**
   * Whether the target is marked as default one.
   *
   * @var int
   */
  protected $default = 1;

  /**
   * The target weight.
   *
   * @var int
   */
  protected $weight;

  /**
   * The preferred language for the target.
   *
   * @var string
   */
  protected $preferredLanguage;

  /**
   * {@inheritdoc}
   */
  public function isDefault() {
    return (bool) $this->default;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreferredLanguage() {
    $langcode = $this->preferredLanguage;

    if (!$langcode) {
      return NULL;
    }

    return $this->languageManager()->getLanguage($langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefix() {
    return $this->prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

}
