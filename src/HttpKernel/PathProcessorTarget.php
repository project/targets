<?php

namespace Drupal\targets\HttpKernel;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\NodeInterface;
use Drupal\targets\TargetInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Processes the inbound path by resolving target in the url.
 */
class PathProcessorTarget implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * PathProcessorTarget constructor.
   *
   * @param \Drupal\targets\TargetManagerInterface $target_manager
   *   The target manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(TargetManagerInterface $target_manager, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->targetManager = $target_manager;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    // Find target in thr path.
    $target = $this->targetManager->getTargetFromPath($path);

    // Return original path if no target found.
    if (!$target) {
      return $path;
    }

    // Ensure there is slash as prefix and suffix.
    $path = trim($path, '/');
    $path = "/{$path}/";

    // Normalize path by removing a target from it.
    // We just need to remove first match if there are multiple in the path.
    $target_path = "/{$target->getPrefix()}/";
    $position = strpos($path, $target_path);
    $path = substr_replace($path, '/', $position, strlen($target_path));

    // We have to return a valid path but / won't be routable and config
    // might be broken so stop execution if fron page is not configured.
    if ($path === '/') {
      $path = $this->configFactory->get('system.site')->get('page.front');

      if (empty($path)) {
        throw new NotFoundHttpException();
      }
    }

    // Remove slash from the path suffix.
    return rtrim($path, '/');
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $target = $this->targetManager->getCurrentTarget($request);

    // No targets created. Use the original path in this case.
    if (is_null($target)) {
      return $path;
    }

    // Don't modify path if the target option in the url is FALSE.
    if (isset($options['target']) && $options['target'] === FALSE) {
      return $path;
    }

    // Get the specific target for the node entity type.
    if (!isset($options['target']) && isset($options['entity']) && $options['entity'] instanceof NodeInterface && isset($options['entity']->get('target')->value)) {
      $node_target_id = $options['entity']->get('target')->value;

      if ($node_target_id) {
        $options['target'] = $this->entityTypeManager
          ->getStorage('target')
          ->load($node_target_id);
      }
    }

    // Use target from the option if it exists.
    $target = $options['target'] ?? $target;

    if (isset($options['language'])) {
      $language_not_defined = in_array($options['language']->getId(), [
        LanguageInterface::LANGCODE_NOT_SPECIFIED,
        LanguageInterface::LANGCODE_NOT_APPLICABLE,
      ]);

      // If the language is not defined, override the config language and
      // set it to the site's default.
      if ($language_not_defined) {
        $options['language'] = $this->languageManager->getDefaultLanguage();
        $options['prefix'] = $options['language']->getId() . '/';
      }

      // Get the right translation of the target entity.
      $target = $this->targetManager->getTranslationFromContext($target, $options['language']);
    }

    if ($bubbleable_metadata) {
      // Cached URLs that have been processed by this outbound path processor
      // must be varied by the content target and the current language.
      $bubbleable_metadata
        ->addCacheContexts(['target', 'languages']);
    }

    // If the path is front page when just redirect to a target.
    $front_path = $this->configFactory->get('system.site')->get('page.front');
    if ($front_path === $path) {
      $path = '/';
    }

    // Return path with prefixed target.
    return '/' . $target->getPrefix() . $path;
  }

}
