<?php

namespace Drupal\targets\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\targets\TargetInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Sets the target property on the language manager.
 */
class TargetEventSubscriber implements EventSubscriberInterface {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * TargetRequestSubscriber constructor.
   *
   * @param \Drupal\targets\TargetManagerInterface $target_manager
   *   The target manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(TargetManagerInterface $target_manager, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $configFactory) {
    $this->targetManager = $target_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Handle this event before "redirect.route_normalizer_request_subscriber".
    $events[KernelEvents::REQUEST][] = ['onKernelRequest', 31];

    // Handle this event after "redirect.route_normalizer_request_subscriber".
    $events[KernelEvents::REQUEST][] = ['checkNodeTarget', 29];

    return $events;
  }

  /**
   * If user visits a naked domain we need to redirect to the default target.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequest(RequestEvent $event) {
    if ($event->isMainRequest()) {
      $request = $event->getRequest();

      // Only check GET requests.
      if ($request->getMethod() !== 'GET') {
        return;
      }

      // Check if there is a target in the request's path.
      $path = $request->getRequestUri();
      $target = $this->targetManager->getTargetFromPath($path);

      // Nothing to do if a target was found in the URL.
      if ($target) {
        return;
      }

      $config = $this->configFactory->get('targets.settings');

      // Do not redirect if the current path is excluded.
      $excluded = $config->get('excluded_paths');
      if (in_array($path, $excluded, TRUE)) {
        return;
      }

      // If no target was found in the URL, get the current one by cookie or use
      // default one if no cookie available.
      $target = $this->targetManager->getCurrentTarget($request);

      // Do nothing if no targets created.
      if (is_null($target)) {
        return;
      }

      $options = [
        'query' => $request->query->all(),
        'target' => $target,
      ];
      $url = Url::fromUserInput($path, $options);

      $response = new RedirectResponse($url->toString(), $config->get('redirect_status_code'));
      $response->send();
      exit;
    }
  }

  /**
   * If user visits a node page within a wrong target, we need to do a redirect.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function checkNodeTarget(RequestEvent $event) {
    if ($event->isMainRequest()) {
      $request = $event->getRequest();

      // Only check node canonical page.
      if ($request->attributes->get('_route') === 'entity.node.canonical') {
        /** @var \Drupal\node\NodeInterface $node */
        $node = $request->attributes->get('node');
        $node_target = $node->get('target')->value;
        $current_target = $this->targetManager->getCurrentTarget($request);

        // Skip redirection if there are no targets created, node target is not
        // set up yet, all targets selected for the node or the current target
        // matches the node target.
        if (!$current_target || is_null($node_target) || $node_target === TargetInterface::ALL_TARGETS || $current_target->id() === $node_target) {
          return;
        }

        $storage = $this->entityTypeManager->getStorage('target');
        $target = $storage->load($node_target);

        // Make sure the target in the node exists.
        if (!$target) {
          return;
        }

        $url = $node->toUrl('canonical', [
          'target' => $target,
        ]);

        // Send redirect response.
        $config = $this->configFactory->get('targets.settings');
        $response = new RedirectResponse($url->toString(), $config->get('redirect_status_code'));
        $response->send();
        exit();
      }
    }
  }

}
