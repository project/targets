<?php

namespace Drupal\targets;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a target entity.
 */
interface TargetInterface extends ConfigEntityInterface {

  /**
   * Define a value which should be used when all targets selected.
   */
  const ALL_TARGETS = '0';

  /**
   * Check if the target is default one.
   *
   * @return bool
   *   TRUE if the target is default one.
   */
  public function isDefault();

  /**
   * Get the preferred language for the target.
   *
   * If the target language matches the current one, the target entity
   * will be a default one.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   The language object or NULL if none language selected.
   */
  public function getPreferredLanguage();

  /**
   * Get the prefix of the target.
   *
   * @return string
   *   The prefix to be used in the url.
   */
  public function getPrefix();

  /**
   * Get the weight of the target.
   *
   * @return int
   *   The weight of the target to be used for ordering multiple targets.
   */
  public function getWeight();

}
