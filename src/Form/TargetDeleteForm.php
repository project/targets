<?php

namespace Drupal\targets\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a confirmation form for deleting a target entity.
 */
class TargetDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Make sure you removed all usages from content such as nodes and menu links. This action cannot be undone.');
  }

}
