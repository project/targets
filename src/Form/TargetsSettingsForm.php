<?php

namespace Drupal\targets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\targets\TargetManagerInterface;

/**
 * Provides a config form for targets functionality.
 */
class TargetsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['targets.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'targets_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('targets.settings');
    $form['redirect_status_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirect status code'),
      '#default_value' => $config->get('redirect_status_code'),
      '#options' => [
        301 => $this->t('301 - moved permanently'),
        302 => $this->t('302 - moved temporarily'),
        307 => $this->t('307 - temporary redirect, not cached'),
      ],
      '#description' => $this->t('This redirect is used when user visits naked domain or a page with wrong target prefix.'),
    ];
    $form['excluded_paths'] = [
      '#type' => 'textarea',
      '#rows' => 10,
      '#title' => $this->t('Paths to exclude'),
      '#description' => $this->t('Enter one path per line, e.g. /node/1 or /path-to-exclude'),
      '#default_value' => implode(PHP_EOL, $config->get('excluded_paths')),
      '#size' => 60,
    ];
    $form['select_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Target select method'),
      '#default_value' => $config->get('select_method'),
      '#options' => [
        TargetManagerInterface::METHOD_COOKIE => $this->t('From a cookie'),
        TargetManagerInterface::METHOD_LANGUAGE => $this->t('From the language context'),
      ],
      '#description' => $this->t('Select how the current target should be determined when no target found in the path.'),
    ];
    $form['cookie_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#default_value' => $config->get('cookie_name'),
      '#description' => $this->t('Provide a name for the cookie to be used.'),
      '#states' => [
        'visible' => [
          ':input[name="select_method"]' => ['value' => TargetManagerInterface::METHOD_COOKIE],
        ],
        'required' => [
          ':input[name="select_method"]' => ['value' => TargetManagerInterface::METHOD_COOKIE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $excluded = preg_split("[\n|\r]", $form_state->getValue('excluded_paths'));
    $excluded = array_filter($excluded);

    $this->config('targets.settings')
      ->set('redirect_status_code', $form_state->getValue('redirect_status_code'))
      ->set('excluded_paths', $excluded)
      ->set('select_method', $form_state->getValue('select_method'))
      ->set('cookie_name', $form_state->getValue('cookie_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
