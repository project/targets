<?php

namespace Drupal\targets\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Target add and edit forms.
 */
class TargetForm extends EntityForm {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * Constructs a TargetForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\targets\TargetManagerInterface $target_manager
   *   The target manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, TargetManagerInterface $target_manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->targetManager = $target_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('targets.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\targets\TargetInterface $target */
    $target = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $target->label(),
      '#description' => $this->t('The target label displayed in links.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $target->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$target->isNew(),
    ];
    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#maxlength' => 255,
      '#default_value' => $target->getPrefix(),
      '#description' => $this->t('The prefix used in the url for content belonging to this target.'),
      '#required' => TRUE,
    ];
    $form['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is default?'),
      '#default_value' => $target->isDefault(),
      '#description' => $this->t('Whether the target is marked as default one. When visiting naked domain user will be redirected to the default target.'),
    ];

    $preferred_language = $target->getPreferredLanguage();
    $form['preferredLanguage'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Preferred language'),
      '#languages' => LanguageInterface::STATE_CONFIGURABLE,
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $preferred_language ? $preferred_language->getId() : '',
      '#description' => $this->t('If the target language matches the current one, the target entity will be a default one when visiting naked domain. This is only applicable if the target select method is a language context.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('target');
    $default_weight = 0;

    if ($targets = $storage->loadMultiple()) {
      $last_target = array_pop($targets);
      $default_weight = $last_target->getWeight() + 1;
    }

    $target = $this->entity;
    $target->set('weight', $default_weight);
    $status = $target->save();

    // If this target is set as default one then load the other default targets
    // and reset its default value.
    $this->targetManager->resetDefaultTarget($target);

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Target created.', [
        '%label' => $target->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Target updated.', [
        '%label' => $target->label(),
      ]));
    }

    $form_state->setRedirect('entity.target.collection');
  }

  /**
   * Helper function to check whether a Target configuration entity exists.
   */
  public function exist($id) {
    return $this->entityTypeManager->getStorage('target')->getQuery()
      ->condition('id', $id)
      ->count()
      ->execute();
  }

}
