<?php

namespace Drupal\targets;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines a service to manage Target entities.
 *
 * @package Drupal\targets
 */
class TargetManager implements TargetManagerInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * TargetManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, LanguageManagerInterface $language_manager, ConfigFactoryInterface $configFactory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function resetDefaultTarget(TargetInterface $target) {
    // Make sure the target is default one.
    if (!$target->isDefault()) {
      return;
    }

    // Fetch previous default targets.
    $storage = $this->entityTypeManager->getStorage('target');
    $ids = $storage->getQuery()
      ->condition('default', 1)
      ->condition('id', $target->id(), '<>')
      ->execute();

    $targets = $storage->loadMultiple($ids);

    // Reset default value for fetched targets.
    foreach ($targets as $default_target) {
      $default_target->set('default', 0);
      $default_target->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultTarget() {
    $storage = $this->entityTypeManager->getStorage('target');
    $targets = $storage->loadByProperties([
      'default' => 1,
    ]);

    return $targets ? reset($targets) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTarget(Request $request = NULL) {
    $storage = $this->entityTypeManager->getStorage('target');
    $request = $request ?? $this->requestStack->getCurrentRequest();

    // First of all the current target is the target from the path.
    $target = $this->getTargetFromPath($request->getRequestUri());
    if ($target) {
      return $target;
    }

    // Get current target from the cookies if the select method is cookie.
    if ($this->isSelectMethodCookie()) {
      $current_target_id = $request->cookies->get($this->getCookieName());
      if ($current_target_id && ($target = $storage->load($current_target_id))) {
        return $target;
      }
    }

    // Get current target from language context.
    if ($this->isSelectMethodLanguage()) {
      $lang = $this->languageManager
        ->getCurrentLanguage()
        ->getId();

      $targets = $storage->loadByProperties([
        'preferredLanguage' => $lang,
      ]);

      if ($targets) {
        return reset($targets);
      }
    }

    // Returns default target if nothing was found.
    return $this->getDefaultTarget();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetFromPath($path = NULL) {
    // Get current path if it's not given.
    if (is_null($path)) {
      $request = $this->requestStack->getCurrentRequest();
      $path = $request->getRequestUri();
    }

    // Remove slash prefix and suffix from the path.
    $path = trim($path, '/');

    // Get first prefix in the path.
    $path_parts = explode('/', $path);

    // Load targets by parts of the path. If the path contains a target prefix,
    // then first match will be used a found target.
    $storage = $this->entityTypeManager->getStorage('target');
    $targets = $storage->loadByProperties([
      'prefix' => $path_parts,
    ]);

    // Handle multiple targets in the path by providing correct sorting and
    // taking first target as the correct one.
    if (count($targets) > 1) {
      usort($targets, function (TargetInterface $a, TargetInterface $b) use ($path_parts) {
        $a_prefix = $a->getPrefix();
        $b_prefix = $b->getPrefix();

        $a_index = array_search($a_prefix, $path_parts);
        $b_index = array_search($b_prefix, $path_parts);

        return (int) $b_index - (int) $a_index;
      });
    }

    return $targets ? reset($targets) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationFromContext(TargetInterface $target, LanguageInterface $language = NULL) {
    // Get default language if the context was not provided.
    $language = $language ?? $this->languageManager->getDefaultLanguage();
    $original = $this->languageManager->getConfigOverrideLanguage();

    // Return the original one as it should be already translated.
    if ($language->getId() === $original->getId()) {
      return $target;
    }

    // Set temporary config language from the context.
    $this->languageManager->setConfigOverrideLanguage($language);

    // Load target entity from scratch in the specific language context.
    $storage = $this->entityTypeManager->getStorage('target');
    $storage->resetCache([$target->id()]);
    $target = $storage->load($target->id());

    // Revert to the original config override language.
    $this->languageManager->setConfigOverrideLanguage($original);

    return $target;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetSelectElement() {
    // Load all targets to generate options.
    $storage = $this->entityTypeManager->getStorage('target');
    $targets = $storage->loadMultiple();
    $options = array_map(function (TargetInterface $target) {
      return $target->label();
    }, $targets);

    return [
      '#type' => 'select',
      '#title' => $this->t('Target'),
      '#description' => $this->t('This content will be visible for the selected target.'),
      '#options' => $options,
      '#empty_option' => $this->t('- All -'),
      '#empty_value' => TargetInterface::ALL_TARGETS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetSwitchLinks(Url $url) {
    $storage = $this->entityTypeManager->getStorage('target');
    $targets = $storage->loadMultiple();

    // No targets found.
    if (!$targets) {
      return NULL;
    }

    $links = [];

    foreach ($targets as $target) {
      $links[$target->id()] = [
        // We need to clone the $url object to avoid using the same one for all
        // links. When the links are rendered, options are set on the $url
        // object, so if we use the same one, they would be set for all links.
        'url' => clone $url,
        'title' => $target->label(),
        'target' => $target,
      ];

      // Check if the target is the same as the active target.
      $current_target = $this->getCurrentTarget();
      if ($current_target->id() === $target->id()) {
        $links[$target->id()]['attributes']['class'][] = 'is-active';
      }
    }

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public function isSelectMethodCookie() {
    $config = $this->configFactory->get('targets.settings');

    return $config->get('select_method') === self::METHOD_COOKIE;
  }

  /**
   * {@inheritdoc}
   */
  public function isSelectMethodLanguage() {
    $config = $this->configFactory->get('targets.settings');

    return $config->get('select_method') === self::METHOD_LANGUAGE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCookieName() {
    $config = $this->configFactory->get('targets.settings');

    return $config->get('cookie_name');
  }

}
