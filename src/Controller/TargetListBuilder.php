<?php

namespace Drupal\targets\Controller;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Target.
 */
class TargetListBuilder extends DraggableListBuilder {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('targets.manager'),
      $container->get('messenger')
    );
  }

  /**
   * Constructs a new LanguageListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage handler class.
   * @param \Drupal\targets\TargetManagerInterface $target_manager
   *   The target manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, TargetManagerInterface $target_manager, MessengerInterface $messenger) {
    parent::__construct($entity_type, $storage);

    $this->targetManager = $target_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Target');
    $header['default'] = $this->t('Default');
    $header['prefix'] = $this->t('Prefix');
    $header['preferred_language'] = $this->t('Preferred language');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();

    $row['default'] = [
      '#type' => 'radio',
      '#parents' => ['targets_default'],
      '#title' => $this->t('Set @title as default', ['@title' => $entity->label()]),
      '#title_display' => 'invisible',
      '#return_value' => $entity->id(),
      '#id' => 'edit-targets-default-' . $entity->id(),
    ];

    if ($entity->isDefault()) {
      $row['default']['#default_value'] = $entity->id();
    }

    $row['prefix'] = [
      '#plain_text' => $entity->getPrefix(),
    ];
    $language = $entity->getPreferredLanguage();
    $row['preferred_language'] = [
      '#plain_text' => $language ? $language->getName() : '-',
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'targets_admin_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form[$this->entitiesKey]['#targets'] = $this->entities;
    $form['actions']['submit']['#value'] = $this->t('Save configuration');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->entities) {
      $form_state->setErrorByName('', $this->t('There are no targets to configure.'));
    }
    elseif (!isset($this->entities[$form_state->getValue('targets_default')])) {
      $form_state->setErrorByName('targets_default', $this->t('Selected default target no longer exists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save the default target if changed.
    $new_id = $form_state->getValue('targets_default');

    if ($new_id != $this->targetManager->getDefaultTarget()->id()) {
      /** @var \Drupal\targets\TargetInterface $target */
      $target = $this->storage->load($new_id);
      $target->set('default', 1);
      $target->save();

      // If the target is set as default one then load the other default targets
      // and reset its default value.
      $this->targetManager->resetDefaultTarget($target);
    }

    $this->messenger->addStatus($this->t('Configuration saved.'));
    // Force the redirection to the page with the language we have just
    // selected as default.
    $form_state->setRedirectUrl($this->entities[$new_id]->toUrl('collection'));
  }

}
