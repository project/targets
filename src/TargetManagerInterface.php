<?php

namespace Drupal\targets;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an interface defining a target manager service.
 *
 * @package Drupal\targets
 */
interface TargetManagerInterface {

  /**
   * The key of the cookie method to detect current target.
   */
  const METHOD_COOKIE = 'cookie';

  /**
   * The key of the language method to detect current target.
   */
  const METHOD_LANGUAGE = 'language';

  /**
   * Reset value for all previous default targets to keep only one as default.
   *
   * @param \Drupal\targets\TargetInterface $target
   *   The default target to keep.
   */
  public function resetDefaultTarget(TargetInterface $target);

  /**
   * Get default target based on the configuration.
   *
   * @return \Drupal\targets\TargetInterface
   *   The target entity or NULL if there are no targets available.
   */
  public function getDefaultTarget();

  /**
   * Get current target from the current request.
   *
   * Use default target if nothing found in the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to check. Leave empty to use the current request.
   *
   * @return \Drupal\targets\TargetInterface
   *   The target entity or NULL if there are no targets available.
   */
  public function getCurrentTarget(Request $request = NULL);

  /**
   * Get select target form element to be built in entity form.
   *
   * @return array
   *   The form element.
   */
  public function getTargetSelectElement();

  /**
   * Get target from the path.
   *
   * Check prefixes to find the correct target.
   *
   * @param string $path
   *   The relative path to check. Leave empty to use current path.
   *
   * @return \Drupal\targets\TargetInterface
   *   The target entity or NULL if no matches found in the path.
   */
  public function getTargetFromPath($path = NULL);

  /**
   * Get translation of the target from the language context.
   *
   * @param \Drupal\targets\TargetInterface $target
   *   The original target config entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The context language. Leave empty to use default site language.
   *
   * @return \Drupal\targets\TargetInterface
   *   The translated target entity.
   */
  public function getTranslationFromContext(TargetInterface $target, LanguageInterface $language = NULL);

  /**
   * Build target switch links.
   *
   * @param \Drupal\Core\Url $url
   *   The current url to build a target link.
   *
   * @return array
   *   Returns targets links or NULL if no target available.
   */
  public function getTargetSwitchLinks(Url $url);

  /**
   * Whether the select method is cookie to detect current target.
   *
   * @return bool
   *   TRUE if the select method cookie.
   */
  public function isSelectMethodCookie();

  /**
   * Whether the select method is language to detect current target.
   *
   * @return bool
   *   TRUE if the select method cookie.
   */
  public function isSelectMethodLanguage();

  /**
   * Get the cookie name to be used to determine current target.
   *
   * @return string
   *   The name of the cookie.
   */
  public function getCookieName();

}
