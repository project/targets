<?php

namespace Drupal\targets\Plugin\views\filter;

use Drupal\targets\TargetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * Provides filtering by target.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("target_filter")
 */
class TargetFilter extends InOperator {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->targetManager = $container->get('targets.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Target Filter');
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    foreach ($this->value as &$value) {
      if ($value === 'current') {
        // Make sure there is any target available.
        if ($target = $this->targetManager->getCurrentTarget()) {
          $value = $target->id();
        }
        break;
      }
    }
    // We also add in "0" as a value, because nodes with 0 are available
    // on any target.
    $this->value[] = TargetInterface::ALL_TARGETS;

    // We also need to handle NULL values when the module is being installed.
    // Create a new OR group to include two different conditions.
    // First condition will be for all existing content without targets set.
    $this->options['group'] = $this->query->setWhereGroup('OR');
    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", NULL, 'IS NULL');

    // Second condition is a filter by target which is IN operator.
    parent::query();
  }

  /**
   * Helper function that generates the options.
   *
   * @return array
   *   Options for the widget.
   */
  public function generateOptions() {
    return [
      'current' => $this->t('Current page target'),
    ] + $this->targetManager->getTargetSelectElement()['#options'];
  }

}
