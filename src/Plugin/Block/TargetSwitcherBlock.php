<?php

namespace Drupal\targets\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Target switcher' block.
 *
 * @Block(
 *  id = "target_switcher",
 *  admin_label = @Translation("Target switcher"),
 * )
 */
class TargetSwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TargetManagerInterface $target_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->targetManager = $target_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('targets.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Currently support only links to front page.
    $url = Url::fromRoute('<front>');
    $links = $this->targetManager->getTargetSwitchLinks($url);

    if ($links) {
      $build = [
        '#theme' => 'links__targets',
        '#links' => $links,
        '#attributes' => [
          'class' => [
            'targets__link',
          ],
        ],
        '#cache' => [
          'tags' => [],
        ],
      ];

      /** @var \Drupal\targets\TargetInterface[] $targets */
      $targets = array_column($links, 'target');

      // Provide cache tags of each target.
      foreach ($targets as $target) {
        $build['#cache']['tags'] = Cache::mergeTags($build['#cache']['tags'], $target->getCacheTags());
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Ensure the block invalidates when we remove or add new target.
    $entity_type = $this->entityTypeManager->getDefinition('target');

    return Cache::mergeTags(parent::getCacheTags(), $entity_type->getListCacheTags());
  }

}
