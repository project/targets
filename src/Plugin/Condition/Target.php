<?php

namespace Drupal\targets\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\targets\TargetManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Target type' condition.
 *
 * @Condition(
 *   id = "target_type",
 *   label = @Translation("Target type")
 * )
 */
class Target extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TargetManagerInterface $target_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->targetManager = $target_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('targets.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = $this->targetManager->getTargetSelectElement()['#options'];

    $form['targets'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Target type'),
      '#options' => $options,
      '#description' => $this->t('Select targets to enforce. If none are selected, all targets will be allowed.'),
      '#default_value' => $this->configuration['targets'],
      '#attributes' => [
        'class' => ['target-type'],
      ],
    ];

    $form['#attached']['library'][] = 'targets/admin';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['targets'] = array_filter($form_state->getValue('targets'));

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $options = $this->targetManager->getTargetSelectElement()['#options'];
    $targets = $this->configuration['targets'];

    if (!$targets || count($options) === count($targets)) {
      return $this->t('All targets');
    }

    // Get labels for the selected options.
    $targets = array_intersect_key($options, $targets);

    return $this->t('The target is @target', ['@target' => implode(', ', $targets)]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    // If there are no target selected and the condition is not negated, we
    // return TRUE because it means all target types are valid.
    if (empty($this->configuration['targets']) && !$this->isNegated()) {
      return TRUE;
    }

    $target = $this->targetManager->getCurrentTarget();

    // If no targets created we also display this block everywhere.
    if (!$target) {
      return TRUE;
    }

    return isset($this->configuration['targets'][$target->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'targets' => [],
    ] + parent::defaultConfiguration();
  }

}
