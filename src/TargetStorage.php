<?php

namespace Drupal\targets;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage controller class for "target" configuration entities.
 */
class TargetStorage extends ConfigEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $entities = parent::loadMultiple($ids);

    // Sort the entities using the entity class's sort() method.
    // See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    uasort($entities, [$this->getEntityType()->getClass(), 'sort']);

    return $entities;
  }

}
