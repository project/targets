<?php

namespace Drupal\targets\Cache;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\targets\TargetManagerInterface;

/**
 * Defines a cache context for urls with target prefix.
 *
 * Cache context ID: 'target'.
 */
class TargetCacheContext implements CacheContextInterface {

  /**
   * The target manager.
   *
   * @var \Drupal\targets\TargetManagerInterface
   */
  protected $targetManager;

  /**
   * TargetCacheContext constructor.
   *
   * @param \Drupal\targets\TargetManagerInterface $target_manager
   *   The target manager.
   */
  public function __construct(TargetManagerInterface $target_manager) {
    $this->targetManager = $target_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return new TranslatableMarkup('Target');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // Fetch the current target.
    $target = $this->targetManager->getCurrentTarget();

    // No targets created.
    if (is_null($target)) {
      return '';
    }

    // The cache context value will be a current target id.
    return $target->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
