/**
 * @file
 * Main JS behaviour for Targets module.
 */

(function (Drupal, drupalSettings) {

  'use strict';

  /**
   * Set up current target in the browser cookie.
   *
   * @type {{attach: Drupal.behaviors.targetsSetCookieBehaviour.attach}}
   */
  Drupal.behaviors.targetsSetCookieBehaviour = {
    attach: function () {
      window.addEventListener('beforeunload', function () {
        // Get current target from the settings.
        const target = drupalSettings.targets.currentTarget;
        const cookieName = drupalSettings.targets.cookieName;

        // The cookie will expire in 1000 days.
        const date = new Date();
        date.setTime(date.getTime() + 86400 * 1000);
        document.cookie = cookieName + '=' + target + '; expires=' + date.toUTCString() + '; path=/';
      });
    }
  };

})(Drupal, drupalSettings);
