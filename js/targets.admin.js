(function ($, Drupal) {

  'use strict';

  /**
   * Provide the summary information for the target type condition.
   */
  Drupal.behaviors.targetTypeConditionSettingsSummary = {
    attach: function () {
      if (typeof $.fn.drupalSetSummary === 'undefined') {
        return;
      }

      $('[data-drupal-selector="edit-visibility-target-type"]').drupalSetSummary(function (context) {
        if ($(context).find('input.target-type:checked').length) {
          return Drupal.t('Restricted');
        }
        else {
          return Drupal.t('Not restricted');
        }
      });
    }
  };

})(jQuery, Drupal);
